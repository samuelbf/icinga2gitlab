# Icinga2gitlab

Send notifications from your [icinga2 monitoring server](https://icinga.com/docs/icinga-2/latest/doc/01-about/) to [Gitlab alerts](https://docs.gitlab.com/ee/operations/incident_management/alerts.html).

## Dependencies

- icinga2
- python ≥ 3.6

## Install

1. Add an HTTP endpoint for Gitlab alerts at https://\<gitlab-server\>/\<group\>/\<project\>/-/settings/operations
2. Put [gitlab-host-notification.py](gitlab-host-notification.py) and [gitlab-service-notification.py](gitlab-service-notification.py) under `/etc/icinga2/scripts`
3. Put [GitlabNotificationCommands.conf](GitlabNotificationCommands.conf) under `/etc/icinga2/conf.d`
4. Define notifications for gitlab (see [Notification.example.conf](Notification.example.conf)) using configuration from step 1.
5. Restart icinga2

## License

This prototype is licensed under GPLv3 terms.
