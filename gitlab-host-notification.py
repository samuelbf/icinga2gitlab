#!/bin/env python3
#
# Copyright © Samuel BF, 2021
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3.
# See LICENSE file for details.

from urllib.parse import quote
from urllib.request import Request, urlopen
from urllib.error import URLError
from datetime import datetime, timezone
import json
import argparse


# Read arguments
parser = argparse.ArgumentParser(description='Send notification to gitlab alerts for host problems.')
parser.add_argument('url', help='gitlab HTTP endpoint (http.../alerts/notify/...)')
parser.add_argument('token', help='gitlab token')
parser.add_argument('laststatechange', help="When the last state change occurred")
parser.add_argument('hostname')
parser.add_argument('hoststate', help='state of the host')
parser.add_argument('hostoutput', help='output from the check')
parser.add_argument('notificationtype')
parser.add_argument('-i', '--icingaweb2url')
parser.add_argument('-e', '--gitlab_environment_name')

args = parser.parse_args()


# Build alert object
post_fields = {}

post_fields['title'] = 'Host '+args.hostname+ ' is '+args.hoststate
post_fields['monitoring_tool'] = 'icinga2'
post_fields['hosts'] = args.hostname
post_fields['description'] = args.hostoutput

if args.icingaweb2url:
    post_fields['description'] += ' / '+args.icingaweb2url+'/monitoring/host/show?host='+quote(args.hostname)

if args.gitlab_environment_name:
    post_fields['gitlab_environment_name'] = args.gitlab_environment_name

post_fields['severity'] = 'critical'
post_fields['fingerprint'] = 'host:'+args.hostname

local_timezone = datetime.now(timezone.utc).astimezone().tzinfo

if args.notificationtype.lower() in ['acknowledgement', 'recovery', 'downtimeend', 'flappingend']:
    post_fields['end_time'] = datetime.fromtimestamp(int(args.laststatechange), tz=local_timezone).isoformat('T')
else:
    post_fields['start_time'] = datetime.fromtimestamp(int(args.laststatechange), tz=local_timezone).isoformat('T')

# Send alert
request = Request(args.url)
request.data = json.JSONEncoder().encode(post_fields).encode('utf-8')
request.add_header('Content-type', 'application/json')
request.add_header('Authorization', 'Bearer ' + args.token)

try:
    result = urlopen(request)
except URLError as e:
    print(e.reason)
    exit(2)

if result.getcode() >= 400:
    print("Failed to send event. Response code " + result.getcode() + " : " + result.read())
    exit(1)
